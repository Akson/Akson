``` c
bool check(list_node * head)
{
    if(head == NULL || head -> next == NULL)
    {
        return true;
    }
    
    list_node * cur = head;
    list_node * two_step = head;
    
    list_node * left = NULL;
    list_node * right = cur -> next;
    
    // find the mid
    while(two_step -> next != NULL && two_step -> next -> next != NULL)
    {
        two_step = two_step -> next -> next;
        
        cur -> next = left;
        left = cur;
        cur = right;
        right = cur -> next;
    }
    
    if(two_step -> next != NULL)
    {
        cur -> next = left;
        left = cur;
    }
    
    while(left != NULL && right != NULL)
    {
        if(left -> val != right -> val)
        {
            return false;
        }
            
        left = left -> next;
        right = right -> next;
    }
    
    return true;
}
```
